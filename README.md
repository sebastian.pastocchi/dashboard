# dashboard

# Dockerfile, repositorio y Balanceador

#Nombre y Apellido: 
Sebastian Pastocchi	
#Materia:
Infraestructura en la nube
#Profesor: 
Sergio Pernas.

#Introducción 
Repositorio Dashboard en GitLab. Este tendrá dockerfile. También se implementará un balanceador entre dos servidores Docker iguales. 

#Requisitos 
Genero 3 instancias como vm. Docker-1, Docker-2 y nginx/Balanceo

#Sistema operativo y red 
- servidor Ubuntu 22.2 
- Red NAT 200.0.0.0/24
- IP 200.0.0.50 Docker-1
- IP 200.0.0.40 Docker-2
- IP 200.0.0.10 Nginx 

#Almacenamiento 
Volumen sda1 para directorio raiz 

#Desarrollo 

#Paso 1 
#En Docker-1, clonamos repositorio “dashborad” :

$ pwd
/home/istea

$ git clone git@gitlab.com:sebastian.pastocchi/dashboard.git

$ ls
dashboard

$ cd dashboard

$ istea@docker-1 dashboard (main) $ git config user.name "John Doe"

$ istea@docker-1 dashboard (main) $ git config user.email john@doe.com

$ git Branch
*main

#Para que nos aparezca en el promt el branch que estamos trabajando cambiar los sig:

$ /home/istea/nano /.bashrc

parse_git_branch() {
git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1="\u@\h \W\[\033[32m\]\$(parse_git_branch)\[\033[00m\] $ "

$ source ~/.bashrc


#Paso 2
#Creamos un repositorio develop

$ git checkout -b develop 
$ git branch
* develop
  main


#Paso 3
#Descargamos archivo de configuracion apache2.conf

$ wget https://gitlab.com/sergio.pernas1/album-jueves/-/raw/main/apache2.conf

#Descargamos el paquete de apps dashboard 

$ wget https://github.com/twbs/bootstrap/releases/download/v5.3.3/bootstrap-5.3.3-examples.zip

#Descomprimir con unzip. Si esto no esta instalado, instalarlo

$ unzip bootstrap-5.3.3-examples.zip

#Movemos la app y assets a un directorio html

$ mv bootstrap-5.3.3-examples/dashboard html
$ mv bootstrap-5.3.3-examples/assets html 

#Una vez descompirmido y copiado borramos el app.zip

$ rm bootstrap-5.3.3-examples.zip -r
$ rm -r bootstrap-5.3.3-examples*


#Paso 4

#Modificamos un titulo al index.html para identificar del Docker 1 al Docker 2


#Paso 5
#Formateamos y montamos un volumen en el Docker agregado previo a la vm

$ lsblk

#sdb 500M
#Formateamos

$ mkfs.xfs /dev/sdb

#Obtenemos el UUID del volumen

$ lsblk -f /dev/sdb
NAME FSTYPE FSVER LABEL UUID                                 FSAVAIL FSUSE% MOUNTPOINTS
sdb  xfs                2d0edd1a-abde-46fe-a1fd-b79b602dd415

#Configuramos montaje automatico en modo systemd 
$ mkdir /mnt/Docker_data

#Configuramos fichero:
$ nano /lib/systemd/System/mnt-docker_data.mount

=============================================
[Unit]
Description=Montaje de la partición de Datos en /mnt/docker_data
After=network.target

[Mount]
What=UUID=2d0edd1a-abde-46fe-a1fd-b79b602dd415
Where=/mnt/docker_data
Type=xfs
Options=defaults

[Install]
WantedBy=multi-user.target
============================================

#Activamos el punto de montaje
$ systemctl enable --now mnt-docker_data.mount 
Created symlink /etc/systemd/system/multi-user.target.wants/mnt-docker_data.mount → /lib/systemd/system/mnt-docker_data.mount.

#Creamos el fichero Daemon.json
$ nano /etc/Docker/Daemon.json
=============================
{
  "data-root": "/mnt/docker_data"
}
===============================

#Iniciamos los servicios 
$ systemctl restart docker
$ systemctl restart docker.socket


#Creamos volumen Docker

$ docker volume create vol_A

 
#Paso 6

#Creamos una imagen Dockerfile

$ nano Dockerfile

=================================================
# imagen base
FROM ubuntu
# ejecutar un comando dentro del contenedor temporal
RUN apt update && apt install apache2 -y

# Copia ficheros y directorios desde fuera hacia adentro
COPY html/ /var/www/html/
COPY apache2.conf /etc/apache2/apache2.conf

# Abre puertos del contenedor
EXPOSE 80 443

# Define el comando o programa que se ejecuta
# cuando se inicia el contenedor
# ['comando', '-opcion', 'argumentos']
CMD ["apache2ctl", "-D", "FOREGROUND"]
======================================================

$ docker build -t dashboard-image .


#Lanzar contenedor

$ docker run \
-d --name dashboard \
--restart=always -p 8080:80 \
-v vol_A:/var/www/html \
-e BRANCHAPP=main \
dashboard-image


#Probamos Acceso

http://14.9.170.102:8080

 
#Paso 7

#Actualizamos repositorio git

$ git add .
$ git commit -m “Docker-app”
$ git push origin main


#Paso 8

#Clonamos vm Docker-01 para armar Docker-02 modificamos hostname e ip

#En docker-02 Lanzar contenedor

$ docker run -d --name dashboard --restart=always -p 8081:80 dashboard-image


#Paso 9
#En la vm nginx configuramos Vhost nginx y balanceo 
$pwd
$/etc/nginx/sites-available

$ nano pagina.net.conf

===========================================
upstream docker-backends {
        server 200.0.0.50:8080;
        server 200.0.0.40:8081;

        # Por defecto el algorimo de balanceo es round robin
        # descomentar para activar
        # ip_hash;
        least_conn;
}

server {
        listen 80;

        server_name pagina-dashboard.net;

        # en este caso la peticion solicita por el raiz
        location / {
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;

                # direccion ip y puerto del servidor que contiene el recurso
                proxy_pass http://docker-backends;
        }
}
============================================

#activamos el vhost

$ ln -s /etc/nginx/sites-available/pagina.net.conf /etc/nginx/sites-enabled/

$ systemctl reload nginx.service


#Agregamos en el Hosts de nuestra maquina (anfitrión)

14.9.170.102	pagina-dashboard.net


#Paso 10

#Probamos con un navegador:
http:// pagina-dashboard.net/


#Paso 11

#Documentamos en en archivo READMI.md y actualizamos el repositorio git

$ git add .
$ git commit -m “Balanceo”
$ git push origin main

#Conclusión (opcional) 
#Esto fue creado en un entorno local con virtualizadores. Creo que seria mejor un entorno en cloud, de todos modos es la misma función #contemplando los DNS que nos proporciona AWS.

#Fuentes 
Enlaces a los sitios de los que se obtuvo información. 
https://gitlab.com/users/sign_in
https://gitlab.com/sergio.pernas1/album-jueves/-/raw/main/apache2.conf
https://github.com/twbs/bootstrap/releases/download/v5.3.3/bootstrap-5.3.3-examples.zip
https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/
https://docs.aws.amazon.com/vpc/latest/userguide/vpc-security-groups.html 
https://docs.nginx.com/nginx/admin-guide/load-balancer/http-load-balancer/ 

#Algunas consideraciones 

#Viabilidad del proyecto 

#El proyecto funciono perfectamente. 

#Veracidad del proyecto 
#Considerando lo anterior todo lo que se vuelque en la documentación debe ser verdad, no se puede falsear resultados. 

# Acceso al trabajo final

https://gitlab.com/sebastian.pastocchi/dashboard/-/blob/main/README.md 
